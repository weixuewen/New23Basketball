package com.sam.basketball.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.sam.android.library.R;
import com.sam.basketball.adapter.WonderShareListAdapter;
import com.sam.ui.base.fragment.BaseFragment;
import com.sam.utils.display.DensityUtils;
import com.sam.widget.slide_banner.SlideBanner;

import java.util.ArrayList;
import java.util.List;

public class WonderSubfragment extends BaseFragment {
    private static final String KEY_IS_WONDER_MOMENT = "isWonderMoment";
    private ListView listView;
    private WonderShareListAdapter adapter;
    private boolean isWonderMoment = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subfragment_wonder);
    }

    @Override
    protected void findView() {
        isWonderMoment = getArgumentBundle().getBoolean(KEY_IS_WONDER_MOMENT);
        listView = (ListView) findViewById(R.id.wonder_lv);
        if(isWonderMoment){
            SlideBanner slideBanner = new SlideBanner(getActivity());
            ListView.LayoutParams lp = new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, DensityUtils.dp2px(getActivity(),100f));
            slideBanner.setLayoutParams(lp);
            slideBanner.setBannerImages(new int[]{R.mipmap.ad_1,R.mipmap.ad_2});
            slideBanner.startSlide(3);
            listView.addHeaderView(slideBanner);
        }
    }

    public void setIsWonderMoment(boolean isWonderMoment){
        getArgumentBundle().putBoolean(KEY_IS_WONDER_MOMENT,isWonderMoment);
    }

    @Override
    protected void initView() {
        isWonderMoment = getArgumentBundle().getBoolean(KEY_IS_WONDER_MOMENT);
        adapter = new WonderShareListAdapter(isWonderMoment);
        listView.setAdapter(adapter);
    }
}
