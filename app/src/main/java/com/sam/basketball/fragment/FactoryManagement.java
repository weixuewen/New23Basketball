package com.sam.basketball.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.sam.android.library.R;
import com.sam.ui.base.fragment.BaseFragment;

public class FactoryManagement extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_factory_management);
    }

    @Override
    protected void findView() {
    }

    @Override
    protected void initView() {
    }
}
