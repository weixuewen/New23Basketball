package com.sam.basketball.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.sam.android.library.R;
import com.sam.ui.base.fragment.BaseFragment;

/**
 * Created by Administrator on 2017/1/18.
 */
public class MeFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_me);
    }

    @Override
    protected void findView() {
    }

    @Override
    protected void initView() {
    }
}
