package com.sam.basketball.fragment;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sam.android.library.R;
import com.sam.ui.base.fragment.BaseFragment;
import com.sam.utils.display.DensityUtils;
import com.sam.widget.slide_banner.SlideBanner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiscoveryFragment extends BaseFragment {
    private ListView menuListView;
    private static final String KEY_MENU_ITEM_BG = "menu_item_bg";
    private static final String KEY_MENU_ITEM_ICON = "menu_item_icon";
    private static final String KEY_MENU_ITEM_TEXT = "menu_item_text";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_discovery);
    }

    @Override
    protected void findView() {
        menuListView = (ListView) findViewById(R.id.discovery_menu);
        SlideBanner slideBanner = new SlideBanner(getActivity());
        slideBanner.setBannerImages(new int[]{R.mipmap.ad_2,R.mipmap.ad_1});
        slideBanner.startSlide(5);
        slideBanner.setLayoutParams(new AbsListView.LayoutParams(-1, DensityUtils.dp2px(getActivity(),100f)));
        menuListView.addHeaderView(slideBanner);
    }

    @Override
    protected void initView() {
        List<Map<String,Object>> data = new ArrayList<>();
        data.add(createMenuItem(R.mipmap.player_nearby_menu_bg,R.mipmap.player_nearby_icon,R.string.player_nearby));
        data.add(createMenuItem(R.mipmap.venus_nearby_menu_bg,R.mipmap.venus_nearby_icon,R.string.venus_nearby));
        data.add(createMenuItem(R.mipmap.basketball_bar_menu_bg,R.mipmap.basketball_bar_icon,R.string.basketball_bar));
        data.add(createMenuItem(R.mipmap.camp_nearby_menu_bg,R.mipmap.camp_nearby_icon,R.string.camp_nearby));
        SimpleAdapter adapter = new SimpleAdapter(getActivity(),data,R.layout.item_discovery_menu_list,
                new String[]{KEY_MENU_ITEM_BG,KEY_MENU_ITEM_ICON,KEY_MENU_ITEM_TEXT},
                new int[] {R.id.menu_bg,R.id.menu_icon,R.id.menu_text});
        menuListView.setAdapter(adapter);
        menuListView.setOnItemClickListener(onItemClickListener);
    }

    private Map<String,Object> createMenuItem(@DrawableRes int bgResId, @DrawableRes int iconResId, @StringRes int menuTextResid){
        Map<String,Object> menuItem = new HashMap<>();
        menuItem.put(KEY_MENU_ITEM_BG,bgResId);
        menuItem.put(KEY_MENU_ITEM_ICON,iconResId);
        menuItem.put(KEY_MENU_ITEM_TEXT,getResources().getString(menuTextResid));
        return menuItem;
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position){
                case 0: // 广告栏
                    Toast.makeText(getActivity(),"你点击了广告栏",Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(getActivity(),"你点击了附近的球员",Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Toast.makeText(getActivity(),"你点击了附近的场馆",Toast.LENGTH_LONG).show();
                    break;
                case 3:
                    Toast.makeText(getActivity(),"你点击了23篮球吧",Toast.LENGTH_LONG).show();
                    break;
                case 4:
                    Toast.makeText(getActivity(),"你点击了附近的训练营",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
}
