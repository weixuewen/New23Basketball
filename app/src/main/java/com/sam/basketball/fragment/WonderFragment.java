package com.sam.basketball.fragment;


import android.support.v4.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.sam.android.library.R;
import com.sam.ui.base.fragment.BaseParentFragment;

public class WonderFragment extends BaseParentFragment {
//    private LeftTextSegmentedRightImageBar header;
    PagerSlidingTabStrip tabs;
    ViewPager pager;
    TextView gchManage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_wonder);
    }

    @Override
    protected void findView() {
//        header = (LeftTextSegmentedRightImageBar) findViewById(R.id.header);
         tabs = (PagerSlidingTabStrip)findViewById(R.id.tabs);
         pager = (ViewPager)findViewById(R.id.pager);
         gchManage = (TextView) findViewById(R.id.gongcanghuaganli);
    }

    @Override
    protected void initView() {
        pager.setOffscreenPageLimit(2);//设置ViewPager的缓存界面数,默认缓存为2
        pager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        tabs.setViewPager(pager);
        setTabsValue();
        listenS();
    }

    private void listenS(){
        gchManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                //注意v4包的配套使用
                Fragment fragment = new FactoryManagement();
                FragmentTransaction transaction = fm.beginTransaction();
                transaction.add(R.id.main_page, fragment);
                transaction.commit();
            }
        });
    }

    private void setTabsValue() {
        // 设置Tab是自动填充满屏幕的
//        tabs.setShouldExpand(true);
        // 设置Tab的分割线是透明的
        tabs.setDividerColor(Color.TRANSPARENT);
        // 设置Tab底部线的高度
        tabs.setUnderlineHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()));
        // 设置Tab Indicator的高度
        tabs.setIndicatorHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));

        // 设置Tab标题文字的大小
//        tabs.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16, dm));
        // 设置Tab Indicator的颜色
        tabs.setIndicatorColor(Color.parseColor("#3f51b5"));//#d83737   #d83737(绿)
        // 设置选中Tab文字的颜色 (这是我自定义的一个方法)
//        tabs.setSelectedTextColor(Color.parseColor("#ffffff"));
        // 取消点击Tab时的背景色
//        tabs.setTabBackground(0);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        private final String[] titles = {"",""};
        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
        @Override
        public int getCount() {
            return titles.length;
        }
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:

                    return new Main1Fragment();
                case 1:

                    return new Main2Fragment();
                default:
                    return null;
            }
        }
    }


}
