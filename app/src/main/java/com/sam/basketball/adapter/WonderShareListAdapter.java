package com.sam.basketball.adapter;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sam.android.library.R;
import com.sam.widget.button.StateBitmapImageButton;
import com.sam.widget.image_switcher.RectImageSwitcher;

public class WonderShareListAdapter extends BaseAdapter {

    private boolean isWonderMoment = true;

    public WonderShareListAdapter(boolean isWonderMoment) {
        this.isWonderMoment = isWonderMoment;
    }

    @Override
    public int getCount() {
        return 30;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            if(isWonderMoment) convertView = View.inflate(parent.getContext(), R.layout.item_wonder_share_listview,null);
            else convertView = View.inflate(parent.getContext(), R.layout.item_friend_share_listview,null);
            viewHolder = new ViewHolder();
            viewHolder.avatar = (StateBitmapImageButton) convertView.findViewById(R.id.avatar);
            viewHolder.userNameTV = (TextView) convertView.findViewById(R.id.user_name);
            viewHolder.locationTV = (TextView) convertView.findViewById(R.id.location);
            viewHolder.timeTV = (TextView) convertView.findViewById(R.id.time);
            viewHolder.phoneTV = (TextView) convertView.findViewById(R.id.phone);
            viewHolder.imgSwitcher = (RectImageSwitcher) convertView.findViewById(R.id.img_switcher);
            viewHolder.favoButton = convertView.findViewById(R.id.favo_button);
            viewHolder.shareButton = convertView.findViewById(R.id.share_button);
            viewHolder.commentButton = convertView.findViewById(R.id.comment_button);
            viewHolder.favoTV = (CheckBox) convertView.findViewById(R.id.favo_cb);
            viewHolder.shareTV = (TextView) convertView.findViewById(R.id.share_tv);
            viewHolder.commentTV = (TextView) convertView.findViewById(R.id.comment_tv);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imgSwitcher.setImageResources(new int[]{R.mipmap.test_1,R.mipmap.test_3,R.mipmap.test_4});
        return convertView;
    }

    private class ViewHolder {
        StateBitmapImageButton avatar;
        TextView userNameTV,locationTV,timeTV,phoneTV;
        RectImageSwitcher imgSwitcher;
        View favoButton,shareButton,commentButton;
        CheckBox favoTV;
        TextView shareTV,commentTV;
    }

}
