package com.sam.basketball.adapter;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.sam.utils.display.DensityUtils;

import java.util.ArrayList;
import java.util.List;
public class TestMenuAdapter extends BaseAdapter {
    List<MenuItem> menuItems = new ArrayList<>();

    public TestMenuAdapter(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public int getCount() {
        return menuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Button btn;
        MenuItem item = menuItems.get(position);
        if(convertView == null){
            btn = new Button(parent.getContext());
            btn.setCompoundDrawablePadding(-DensityUtils.dp2px(parent.getContext(),15f));
            btn.setPadding(0,DensityUtils.dp2px(parent.getContext(),25f),0,0);
            convertView = btn;
        } else {
            btn = (Button) convertView;
        }
        btn.setBackgroundResource(item.menuBgResId);
        Drawable topDrawable = parent.getContext().getResources().getDrawable(item.menuIconResId);
        topDrawable.setBounds(0,0,topDrawable.getMinimumWidth(),topDrawable.getMinimumHeight());
        btn.setCompoundDrawables(null,topDrawable,null,null);
        btn.setText(item.menuTextId);
        return convertView;
    }

    class MenuItem {
        @DrawableRes int menuBgResId,menuIconResId;
        @StringRes int menuTextId;
    }
}
