package com.sam.basketball.test;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.sam.android.library.R;
import com.sam.widget.headerBar.BaseHeaderView;

/**
 * Created by Administrator on 2017/1/18.
 */
public class TestHeaderViewActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_header_view);
    }
}
