package com.sam.basketball.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sam.android.library.R;
import com.sam.ui.base.activity.splash_activity.BaseSplashActivity;

/**
 * Created by Administrator on 2017/1/18.
 */
public class SplashActivity extends BaseSplashActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void startGuideActivity() {
        startActivity(new Intent(this, GuideActivity.class));
    }

    @Override
    protected void startMainActivity() {
        startActivity(new Intent(this, MainFrameActivity.class));
    }
}
