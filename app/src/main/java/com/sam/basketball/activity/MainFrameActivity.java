package com.sam.basketball.activity;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.sam.android.library.R;
import com.sam.basketball.fragment.DiscoveryFragment;
import com.sam.basketball.fragment.MeFragment;
import com.sam.basketball.fragment.MessageFragment;
import com.sam.basketball.fragment.WonderFragment;
import com.sam.ui.base.activity.BaseFragmentActivity;
import com.sam.utils.display.DensityUtils;
import com.sam.widget.button.StateBitmapImageButton;
import com.sam.widget.nav_bar.NavigationBar;

public class MainFrameActivity extends BaseFragmentActivity {
    @Override
    protected void initPageInfo() {
        NavigationBar.NavigationItemEntity[] itemEntities = new NavigationBar.NavigationItemEntity[]{
                new NavigationBar.NavigationItemEntity(this, R.string.nav_wonder,R.mipmap.home,R.mipmap.home1),
                new NavigationBar.NavigationItemEntity(this, R.string.nav_discovery,R.mipmap.work,R.mipmap.work1),
                null,
                new NavigationBar.NavigationItemEntity(this, R.string.nav_message,R.mipmap.address,R.mipmap.address1),
                new NavigationBar.NavigationItemEntity(this, R.string.nav_me,R.mipmap.person,R.mipmap.person1)
        };
        Fragment[] pages =  new Fragment[]{
                new WonderFragment(),new DiscoveryFragment(),null,new MessageFragment(),new MeFragment()
        };
        setPageInfo(itemEntities,pages);
        setNavigationBarAttrs(R.color.nav_bg_color,R.dimen.nav_padding,R.dimen.nav_padding,R.dimen.nav_icon_text_padding,R.color.nav_text_uncheck_color,R.color.nav_text_checked_color);
        setNavigationBarTranslucent(true);

        initBall();
    }

    private void initBall(){
        StateBitmapImageButton ball = new StateBitmapImageButton(this);
        ball.setImageResource(R.mipmap.code);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        ball.setLayoutParams(lp);
        ball.setPadding(0,0,0, DensityUtils.dp2px(this,8f));
        getLayoutView().addView(ball);
        ball.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 球点击功能
            }
        });
    }
}
