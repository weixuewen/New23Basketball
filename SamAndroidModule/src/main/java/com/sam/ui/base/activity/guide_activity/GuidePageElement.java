package com.sam.ui.base.activity.guide_activity;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sam.library.R;

public class GuidePageElement extends ImageView{

    private static final int TRANS_ANI_NONE = 0;
    private static final int TRANS_ANI_LEFT_OUT = 1;
    private static final int TRANS_ANI_RIGHT_OUT = 2;
    private static final int TRANS_ANI_TOP_OUT = 3;
    private static final int TRANS_ANI_BOTTOM_OUT = 4;
    private static final int TRANS_ANI_LEFT_TOP_OUT = 5;
    private static final int TRANS_ANI_LEFT_BOTTOM_OUT = 6;
    private static final int TRANS_ANI_RIGHT_TOP_OUT = 7;
    private static final int TRANS_ANI_RIGHT_BOTTOM_OUT = 8;

    private static final int ALPHA_ANI_NONE = 0;
    private static final int ALPHA_ANI_DISAPPEAR = 1;
    private static final int ALPHA_ANI_APPEAR = 2;

    public float xStart = 0;
    public float xEnd = 700;
    public float yStart = 0;
    public float yEnd = 700;

    public float alphaStart = 1.0f;
    public float alphaEnd = 1.0f;
    public Matrix matrix = new Matrix();
    public Paint paint = new Paint();

    private int translateAniType = TRANS_ANI_NONE;
    private int alphaAniType = ALPHA_ANI_NONE;

    public GuidePageElement(Context context) {
        this(context,null);
    }

    public GuidePageElement(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public GuidePageElement(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttrs(attrs);
    }

    private void getAttrs(AttributeSet attrs){
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.GuidePageElement);
        translateAniType = ta.getInt(R.styleable.GuidePageElement_translateAniType,translateAniType);
        alphaAniType = ta.getInt(R.styleable.GuidePageElement_alphaAniType,alphaAniType);
        ta.recycle();
    }

    public void initAniInfo(){
        int[] location = new int[2];
        getLocationInWindow(location);
        xStart = location[0];
        yStart = location[1];
        Log.d("SamLog","当前元素位置：("+location[0]+","+location[1]+")");

        final int selfWidth = getWidth();
        final int selfHeight = getHeight();
        final ViewGroup parentView = (ViewGroup) getParent();
        final int parentWidth = parentView.getWidth();
        final int parentHeight = parentView.getHeight();

        Log.d("SamLog","selfWidth = "+selfWidth+",selfHeight = "+selfHeight+",parentWidth = "+parentWidth+",parentHeight = "+parentHeight);

        switch (translateAniType){
            case  TRANS_ANI_NONE:
                xEnd = xStart;
                yEnd = yStart;
                break;
            case TRANS_ANI_LEFT_OUT:
                xEnd = -selfWidth;
                yEnd = yStart;
                break;
            case TRANS_ANI_RIGHT_OUT:
                xEnd = parentWidth;
                yEnd = yStart;
                break;
            case TRANS_ANI_TOP_OUT:
                xEnd = xStart;
                yEnd = -selfHeight;
                break;
            case TRANS_ANI_BOTTOM_OUT:;
                xEnd = xStart;
                yEnd = parentHeight;
                break;
            case TRANS_ANI_LEFT_TOP_OUT:
                xEnd = -selfWidth;
                yEnd = -selfHeight;
                break;
            case TRANS_ANI_LEFT_BOTTOM_OUT:
                xEnd = -selfWidth;
                yEnd = parentHeight;
                break;
            case TRANS_ANI_RIGHT_TOP_OUT:
                xEnd = parentWidth;
                yEnd = -selfHeight;
                break;
            case TRANS_ANI_RIGHT_BOTTOM_OUT:
                xEnd = parentWidth;
                yEnd = parentHeight;
                break;
        }

        switch (alphaAniType){
            case ALPHA_ANI_NONE:
                alphaStart = alphaEnd = 1.0f;
                break;
            case ALPHA_ANI_APPEAR:
                alphaStart = 0;
                alphaEnd = 1.0f;
                break;
            case ALPHA_ANI_DISAPPEAR:
                alphaStart = 1.0f;
                alphaEnd = 0;
                break;
        }
    }

    public Bitmap getImageBitmap(){
        Drawable srcDrawable = getDrawable();
        if(srcDrawable instanceof BitmapDrawable){
            BitmapDrawable bitmapDrawable = (BitmapDrawable) srcDrawable;
            return bitmapDrawable.getBitmap();
        } else throw new ClassCastException("你必须要为GuidePageElement设置src属性，用图片资源作为动画元素");
    }
}
