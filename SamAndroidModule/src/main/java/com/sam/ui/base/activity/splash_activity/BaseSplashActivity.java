package com.sam.ui.base.activity.splash_activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseSplashActivity extends AppCompatActivity {
    private static final String KEY_IS_FIRST_RUN = "key_is_first_run";
    private static final long SPLASH_DURATION_TIME = 2000;

    private Handler handler = new Handler();

    private void setNotFirstRun(Context context){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(KEY_IS_FIRST_RUN,false);
        editor.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        if(sp.getBoolean(KEY_IS_FIRST_RUN,true) == true){
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startGuideActivity();
                    setNotFirstRun(BaseSplashActivity.this);
                }
            },SPLASH_DURATION_TIME);
        } else {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startMainActivity();
                }
            },SPLASH_DURATION_TIME);

        }
    }

    protected abstract void startGuideActivity();
    protected abstract void startMainActivity();
}
