package com.sam.ui.base.activity.guide_activity;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

public class FragmentTabAdapter extends FragmentPagerAdapter {

    private List<GuidePageLayout> mGuideContent;
    private Context mCtx;

    public FragmentTabAdapter(FragmentActivity fragmentActivity, List<GuidePageLayout> guideContent) {
        super(fragmentActivity.getSupportFragmentManager());
        mCtx = fragmentActivity;
        mGuideContent = guideContent;
    }

    @Override
    public Fragment getItem(int position) {
        // Get a local reference
        GuidePageLayout sp = mGuideContent.get(position);
        if (sp.customFragment != null) { // This single page has custom fragment, use it
            return sp.customFragment;
        } else {
            PageFragment pageFragment = (PageFragment) Fragment.instantiate(mCtx, PageFragment.class.getName());
            if (sp.background != null) {
                pageFragment.setBackgroundDrawable(sp.background);
            }
            return pageFragment;
        }
    }

    @Override
    public int getCount() {
        return mGuideContent.size();
    }

    public static final class PageFragment extends Fragment {

        private Drawable mBg;

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            ImageView iv = new ImageView(getActivity());
            if (mBg != null) {
                iv.setBackgroundDrawable(mBg);
            }
            return iv;
        }

        public void setBackgroundDrawable(Drawable mBackground) {
            mBg = mBackground;
        }

    }
}
