package com.sam.ui.base.activity.guide_activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import java.util.List;

public class GuidePagerView extends View implements ViewPager.OnPageChangeListener{
    private List<GuidePageLayout> mGuideContent;

    private int mPosition;
    private float mOffset;

    private boolean mDrawDot;

    private List<GuidePageElement> mDotList;
    private GuidePageElement mSelectedDot;

    /**
     * This class could not instantiation from XML
     * @param context
     * @param guideContent
     */
    public GuidePagerView(Context context, final List<GuidePageLayout> guideContent) {
        super(context);
        mGuideContent = guideContent;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(mGuideContent == null || mGuideContent.size() == 0) return;
        GuidePageLayout guidePageLayout = mGuideContent.get(mPosition);
        List<GuidePageElement> aniElements = guidePageLayout.getAniElements();
        if (aniElements == null) {
            // No stuff
            super.onDraw(canvas);
            return;
        }
        // Draw custom stuff
        for (int i = 0; i < aniElements.size(); i++) {
            GuidePageElement e = aniElements.get(i);
            drawElement(canvas, e);
        }
        super.onDraw(canvas);
    }

    private void drawElement(Canvas canvas, GuidePageElement element) {
        Bitmap bitmap = element.getImageBitmap();
        Matrix matrix = element.matrix;
        Paint paint = element.paint;

        float dx = element.xStart + (element.xEnd - element.xStart) * mOffset;
        float dy = element.yStart + (element.yEnd - element.yStart) * mOffset;
        float alpha = element.alphaStart + (element.alphaEnd - element.alphaStart) * mOffset;
        Log.d("SamLog","dx = "+dx+",dy = "+dy+",alpha = "+alpha+", mOffset = "+mOffset);

        matrix.setTranslate(dx, dy);
        paint.setAlpha((int) (0xFF * alpha));
        canvas.drawBitmap(bitmap, matrix, paint);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageScrolled(int index, float offset, int offsetPixel) {
        mPosition = index;
        mOffset = offset;// == 0 ? 1.0f : offset;
        invalidate();
    }

    @Override
    public void onPageSelected(int index) {
//        if(index == mGuideContent.size() - 1 && onLastPageClickListener!= null){
//            setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onLastPageClickListener.onLastPageClicked();
//                }
//            });
//        }

    }

//    private OnLastPageClickListener onLastPageClickListener;
//    public interface OnLastPageClickListener{
//        void onLastPageClicked();
//    }
//    public void setOnLastPageClickListener(OnLastPageClickListener onLastPageClickListener) {
//        this.onLastPageClickListener = onLastPageClickListener;
//    }

}
