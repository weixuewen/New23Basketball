package com.sam.ui.base.activity.guide_activity;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sam.library.R;

import java.util.ArrayList;
import java.util.List;

public class GuidePageLayout extends RelativeLayout{
    private ImageView bgImageView;
    /**
     * 引导页单个页面的背景图片
     */
    public Drawable background;
    /**
     * 引导页上浮动的元素，这些元素可以应用动画
     */
    private List<GuidePageElement> elementsList = new ArrayList<>();
    /**
     * 如果不想通过上面的背景图片和元素来组合成一个引导页的单页面，也可以直接通过赋值给下面这个Fragment来设置Fragment作为单页面
     */
    public Fragment customFragment;

    public GuidePageLayout(Context context) {
        this(context,null);
    }

    public GuidePageLayout(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public GuidePageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttrs(attrs);
        init();
    }

    private void getAttrs(AttributeSet attrs){
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.GuidePageLayout);
        background = ta.getDrawable(R.styleable.GuidePageLayout_guidePageBackground);
        ta.recycle();
    }
    private void init(){
        if(background != null) {
            bgImageView = new ImageView(getContext());
            bgImageView.setImageDrawable(background);
            bgImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            WindowManager mWindowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics outMetrics = new DisplayMetrics();
            mWindowManager.getDefaultDisplay().getMetrics(outMetrics);
            if(isInEditMode()){
                LayoutParams lp = new LayoutParams(outMetrics.widthPixels,outMetrics.heightPixels);
                bgImageView.setLayoutParams(lp);
                addView(bgImageView);
            }
        }
    }

    /**
     * 得到所有可以有动画效果的元素列表
     * @return
     */
    public List<GuidePageElement> getAniElements(){
        int count = getChildCount();
        if(count == 0) return null;
        for(int i = 0; i < count; i++){
            View child = getChildAt(i);
            if(child instanceof GuidePageElement){
                GuidePageElement element = (GuidePageElement) child;
                element.initAniInfo();
                elementsList.add(element);
            }
        }
        return elementsList;
    }

    public View getLayoutViewWithoutAniElements(){
        int count = getChildCount();
        if(count == 0) return null;
        for(int i = 0; i < count; i++){
            View child = getChildAt(i);
            if(child instanceof GuidePageElement){
                child.setVisibility(GONE);
            }
        }
        return this;
    }

    public ImageView getBackgroundImageView(){
        return bgImageView;
    }
}
