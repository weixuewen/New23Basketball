package com.sam.ui.base.activity.guide_activity;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sam.library.R;

import java.util.ArrayList;
import java.util.List;

public class GuideViewFrame extends FrameLayout {
    private GuidePagerView guideView;

    public GuideViewFrame(Context context) {
        this(context,null);
    }

    public GuideViewFrame(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    boolean initialized = false;
    public GuideViewFrame(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(!initialized){
                    initGuidePages();
                    initialized = true;
                }
            }
        });
    }

    public synchronized void initGuidePages(){
        ViewPager pager = new ViewPager(getContext());
        pager.setId(R.id.guide_container);
        addView(pager, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        guideView = new GuidePagerView(getContext(), getGuidePages());
        pager.setAdapter(new GuidePageApdapter());
        pager.setOnPageChangeListener(guideView);
        addView(guideView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    /**
     * 得到引导页面列表
     */
    public List<GuidePageLayout> getGuidePages(){
        List<GuidePageLayout> pages = new ArrayList<>();
        int count = getChildCount();
        if(count == 0) return null;
        for(int i = 0; i < count; i++){
            View child = getChildAt(i);
            if(child instanceof GuidePageLayout){
                pages.add((GuidePageLayout) child);
            }
        }
        return pages;
    }



    private class GuidePageApdapter extends PagerAdapter{
        private List<View> pages = new ArrayList<>();

        public GuidePageApdapter() {
            List<GuidePageLayout> pageLayouts = getGuidePages();
            if(pageLayouts == null || pageLayouts.size() == 0) return;
            for(GuidePageLayout guidePageLayout : pageLayouts){
                RelativeLayout page = new RelativeLayout(getContext());
                page.setLayoutParams(new ViewPager.LayoutParams());
                ImageView imageView = guidePageLayout.getBackgroundImageView();
                imageView.setLayoutParams(new ViewPager.LayoutParams());
                page.addView(imageView);
                View pageView = guidePageLayout.getLayoutViewWithoutAniElements();
                ((ViewGroup)pageView.getParent()).removeView(pageView);
                pageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
                page.addView(pageView);
                pages.add(page);
            }
        }

        @Override
        public int getCount() {
            return pages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager)container).removeView(pages.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager)container).addView(pages.get(position));
            return pages.get(position);
        }
    }
}
