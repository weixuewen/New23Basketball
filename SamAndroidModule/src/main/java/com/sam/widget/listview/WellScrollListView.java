package com.sam.widget.listview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

public class WellScrollListView extends ListView {
    public WellScrollListView(Context context) {
        super(context);
    }

    public WellScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WellScrollListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    float xDistance,yDistance;
    float startX,startY;
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                startX = ev.getX();
                startY = ev.getY();
                xDistance = 0;
                yDistance = 0;
                break;
            case MotionEvent.ACTION_MOVE:
                xDistance += Math.abs(ev.getX() - startX);
                yDistance += Math.abs(ev.getY() - startY);
                Log.d("SamLog","xDistance = "+xDistance+"   yDistance = "+yDistance);
                startX = ev.getX();
                startY = ev.getY();
                break;
        }

        if(xDistance > yDistance){
            Log.d("SamLog","滑动图片");
            return false;
        } else Log.d("SamLog","滑动列表");
        return super.onInterceptTouchEvent(ev);
    }
}
