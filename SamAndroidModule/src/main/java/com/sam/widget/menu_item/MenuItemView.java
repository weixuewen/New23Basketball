package com.sam.widget.menu_item;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sam.library.R;
import com.sam.widget.interfaces.StyleSettable;

public class MenuItemView extends RelativeLayout implements StyleSettable{

    private boolean isShowTopBorder = true;
    private boolean isShowBottomBorder = true;
    private int borderColor = Color.GRAY;
    private int borderHeight = 1;
    private int menuLeftIconPaddingLeft = 25;
    private int menuRightIconPaddingRight = 25;
    private Drawable menuLeftIconDrawable;
    private Drawable menuRightIconDrawable;
    private int menuLeftIconDrawablePadding = 25;
    private int menuRightIconDrawablePadding= 25;
    private String menuLeftText;
    private String menuRightText;
    private int menuLeftTextSize = 32;
    private int menuRightTextSize = 32;
    private int menuLeftTextColor = Color.BLACK;
    private int menuRightTextColor = Color.BLACK;

    private View topBorder,bottomBorder;
    private TextView leftTV,rightTV;

    public MenuItemView(Context context) {
        this(context,null);
    }

    public MenuItemView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MenuItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        isShowTopBorder = getResources().getBoolean(R.bool.default_is_show_bottom_border);
        isShowBottomBorder = getResources().getBoolean(R.bool.default_is_show_bottom_border);
        borderColor = getResources().getColor(R.color.default_item_border_color);
        borderHeight = getResources().getDimensionPixelOffset(R.dimen.default_item_border_height);
        menuLeftIconPaddingLeft = getResources().getDimensionPixelOffset(R.dimen.default_menu_left_icon_padding_left);
        menuRightIconPaddingRight = getResources().getDimensionPixelOffset(R.dimen.default_menu_right_icon_padding_right);
        menuLeftText = getResources().getString(R.string.default_menu_left_text);
        menuRightText = getResources().getString(R.string.default_menu_right_text);
        menuLeftTextSize = getResources().getDimensionPixelSize(R.dimen.default_menu_left_text_size);
        menuRightTextSize = getResources().getDimensionPixelSize(R.dimen.default_menu_right_text_size);
        menuLeftTextColor = getResources().getColor(R.color.default_menu_left_text_color);
        menuRightTextColor = getResources().getColor(R.color.default_menu_right_text_color);

        TypedArray ta = context.obtainStyledAttributes(attrs,R.styleable.MenuItemView,R.attr.menuItemViewStyle,R.style.Default_MenuItemView_Style);
        resolveAttrs(ta);
        initView();
        refreshView();
    }


    @Override
    public void setStyle(@StyleRes int i) {
        resolveAttrs(getContext().obtainStyledAttributes(i,R.styleable.MenuItemView));
        refreshView();
    }

    @Override
    public void resolveAttrs(TypedArray typedArray) {
        isShowTopBorder = typedArray.getBoolean(R.styleable.MenuItemView_isShowTopBorder,isShowTopBorder);
        if(typedArray.hasValue(R.styleable.MenuItemView_isShowBottomBorder))
        isShowBottomBorder = typedArray.getBoolean(R.styleable.MenuItemView_isShowBottomBorder,isShowBottomBorder);
        borderColor = typedArray.getColor(R.styleable.MenuItemView_itemBorderColor,borderColor);
        borderHeight = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_itemBorderHeight,borderHeight);
        menuLeftIconPaddingLeft = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_menuLeftIconPaddingLeft,menuLeftIconPaddingLeft);
        menuRightIconPaddingRight = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_menuRightIconPaddingRight,menuRightIconPaddingRight);
        menuLeftText = typedArray.getString(R.styleable.MenuItemView_menuLeftText);
        menuRightText = typedArray.getString(R.styleable.MenuItemView_menuLeftText);
        menuLeftTextSize = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_menuLeftTextSize,menuLeftTextSize);
        menuRightTextSize = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_menuRightTextSize,menuRightTextSize);
        menuLeftTextColor = typedArray.getColor(R.styleable.MenuItemView_menuLeftTextColor,menuLeftTextColor);
        menuRightTextColor = typedArray.getColor(R.styleable.MenuItemView_menuRightTextColor,menuRightTextColor);

        menuLeftIconDrawable = typedArray.getDrawable(R.styleable.MenuItemView_menuLeftIconDrawable);
        menuLeftIconDrawablePadding = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_menuLeftIconDrawablePadding,menuLeftIconDrawablePadding);
        menuRightIconDrawable = typedArray.getDrawable(R.styleable.MenuItemView_menuRightIconDrawable);
        menuRightIconDrawablePadding = typedArray.getDimensionPixelOffset(R.styleable.MenuItemView_menuRightIconDrawablePadding,menuRightIconDrawablePadding);
    }

    private void initView(){
        topBorder = new View(getContext());
        LayoutParams lp1 = new LayoutParams(LayoutParams.MATCH_PARENT,borderHeight);
        lp1.addRule(ALIGN_PARENT_TOP);
        topBorder.setLayoutParams(lp1);
        addView(topBorder);

        bottomBorder = new View(getContext());
        LayoutParams lp2 = new LayoutParams(LayoutParams.MATCH_PARENT,borderHeight);
        lp2.addRule(ALIGN_PARENT_BOTTOM);
        bottomBorder.setLayoutParams(lp2);
        addView(bottomBorder);

        leftTV = new TextView(getContext());
        LayoutParams lp3 = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        lp3.setMargins(menuLeftIconPaddingLeft,0,0,0);
        lp3.addRule(CENTER_VERTICAL);
        leftTV.setLayoutParams(lp3);
        leftTV.setGravity(Gravity.CENTER_VERTICAL);
        addView(leftTV);

        rightTV = new TextView(getContext());
        LayoutParams lp4 = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        lp4.addRule(CENTER_VERTICAL);
        lp4.addRule(ALIGN_PARENT_RIGHT);
        lp4.setMargins(0,0,menuRightIconPaddingRight,0);
        leftTV.setGravity(Gravity.CENTER_VERTICAL);
        rightTV.setLayoutParams(lp4);
        addView(rightTV);
    }

    @Override
    public void refreshView() {
        setShowTopBorder(isShowTopBorder);
        setShowBottomBorder(isShowBottomBorder);
        setBorderColor(borderHeight);
        setBorderHeight(borderHeight);
        setMenuLeftText(menuLeftText);
        setMenuLeftTextColor(menuLeftTextColor);
        leftTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,menuLeftTextSize);

        if(menuLeftIconDrawable != null) {
            menuLeftIconDrawable.setBounds(0,0,menuLeftIconDrawable.getMinimumWidth(),menuLeftIconDrawable.getMinimumHeight());
            leftTV.setCompoundDrawables(menuLeftIconDrawable,null,null,null);
            leftTV.setCompoundDrawablePadding(menuLeftIconDrawablePadding);
        }

        setMenuRightText(menuRightText);
        setMenuRightTextColor(menuRightTextColor);
        rightTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,menuRightTextSize);
        if(menuRightIconDrawable != null) {
            menuRightIconDrawable.setBounds(0,0,menuRightIconDrawable.getMinimumWidth(),menuRightIconDrawable.getMinimumHeight());
            rightTV.setCompoundDrawables(menuRightIconDrawable,null,null,null);
            rightTV.setCompoundDrawablePadding(menuRightIconDrawablePadding);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if(heightMode == MeasureSpec.AT_MOST){
            int height = (int) (getResources().getDisplayMetrics().density * 60);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height,MeasureSpec.EXACTLY);
    }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }


    //===================================================================================
    //                   GETTER AND SETTER
    //===================================================================================

    public boolean isShowTopBorder() {
        return isShowTopBorder;
    }

    public void setShowTopBorder(boolean showTopBorder) {
        isShowTopBorder = showTopBorder;
        topBorder.setVisibility(isShowTopBorder ? VISIBLE : GONE);
    }

    public boolean isShowBottomBorder() {
        return isShowBottomBorder;
    }

    public void setShowBottomBorder(boolean showBottomBorder) {
        isShowBottomBorder = showBottomBorder;
        bottomBorder.setVisibility(isShowBottomBorder ? VISIBLE : GONE);
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
        topBorder.setBackgroundColor(borderColor);
        bottomBorder.setBackgroundColor(borderColor);
    }

    public int getBorderHeight() {
        return borderHeight;
    }

    public void setBorderHeight(int borderHeight) {
        this.borderHeight = borderHeight;
        LayoutParams lp1 = (LayoutParams) topBorder.getLayoutParams();
        lp1.height = borderHeight;
        topBorder.setLayoutParams(lp1);

        LayoutParams lp2 = (LayoutParams) bottomBorder.getLayoutParams();
        lp2.height = borderHeight;
        bottomBorder.setLayoutParams(lp2);
    }

    public int getMenuLeftIconPaddingLeft() {
        return menuLeftIconPaddingLeft;
    }

    public void setMenuLeftIconPaddingLeft(int menuLeftIconPaddingLeft) {
        this.menuLeftIconPaddingLeft = menuLeftIconPaddingLeft;
        LayoutParams lp1 = (LayoutParams) leftTV.getLayoutParams();
        lp1.setMargins(menuLeftIconPaddingLeft,0,0,0);
        leftTV.setLayoutParams(lp1);
    }

    public int getMenuRightIconPaddingRight() {
        return menuRightIconPaddingRight;
    }

    public void setMenuRightIconPaddingRight(int menuRightIconPaddingRight) {
        this.menuRightIconPaddingRight = menuRightIconPaddingRight;
        LayoutParams lp1 = (LayoutParams) rightTV.getLayoutParams();
        lp1.setMargins(0,0,menuRightIconPaddingRight,0);
        rightTV.setLayoutParams(lp1);
    }

    public Drawable getMenuLeftIconDrawable() {
        return menuLeftIconDrawable;
    }

    public void setMenuLeftIconDrawable(Drawable menuLeftIconDrawable) {
        this.menuLeftIconDrawable = menuLeftIconDrawable;
        menuLeftIconDrawable.setBounds(0,0,menuLeftIconDrawable.getMinimumWidth(),menuLeftIconDrawable.getMinimumHeight());
        leftTV.setCompoundDrawables(menuLeftIconDrawable,null,null,null);
    }

    public Drawable getMenuRightIconDrawable() {
        return menuRightIconDrawable;
    }

    public void setMenuRightIconDrawable(Drawable menuRightIconDrawable) {
        this.menuRightIconDrawable = menuRightIconDrawable;
        menuRightIconDrawable.setBounds(0,0,menuRightIconDrawable.getMinimumWidth(),menuRightIconDrawable.getMinimumHeight());
        rightTV.setCompoundDrawables(menuRightIconDrawable,null,null,null);
    }

    public int getMenuLeftIconDrawablePadding() {
        return menuLeftIconDrawablePadding;
    }

    public void setMenuLeftIconDrawablePadding(int menuLeftIconDrawablePadding) {
        this.menuLeftIconDrawablePadding = menuLeftIconDrawablePadding;
        leftTV.setCompoundDrawablePadding(menuLeftIconPaddingLeft);
    }

    public int getMenuRightIconDrawablePadding() {
        return menuRightIconDrawablePadding;
    }

    public void setMenuRightIconDrawablePadding(int menuRightIconDrawablePadding) {
        this.menuRightIconDrawablePadding = menuRightIconDrawablePadding;
        rightTV.setCompoundDrawablePadding(menuRightIconPaddingRight);
    }

    public String getMenuLeftText() {
        return menuLeftText;
    }

    public void setMenuLeftText(String menuLeftText) {
        this.menuLeftText = menuLeftText;
        leftTV.setText(menuLeftText);
    }

    public String getMenuRightText() {
        return menuRightText;
    }

    public void setMenuRightText(String menuRightText) {
        this.menuRightText = menuRightText;
        rightTV.setText(menuRightText);
    }

    public int getMenuLeftTextSize() {
        return menuLeftTextSize;
    }

    public void setMenuLeftTextSize(int menuLeftTextSize) {
        this.menuLeftTextSize = menuLeftTextSize;
        leftTV.setTextSize(menuLeftTextSize);
    }

    public int getMenuRightTextSize() {
        return menuRightTextSize;
    }

    public void setMenuRightTextSize(int menuRightTextSize) {
        this.menuRightTextSize = menuRightTextSize;
        rightTV.setTextScaleX(menuRightTextSize);
    }

    public int getMenuLeftTextColor() {
        return menuLeftTextColor;
    }

    public void setMenuLeftTextColor(int menuLeftTextColor) {
        this.menuLeftTextColor = menuLeftTextColor;
        leftTV.setTextColor(menuLeftTextColor);
    }

    public int getMenuRightTextColor() {
        return menuRightTextColor;
    }

    public void setMenuRightTextColor(int menuRightTextColor) {
        this.menuRightTextColor = menuRightTextColor;
        rightTV.setTextColor(menuRightTextColor);
    }
}
